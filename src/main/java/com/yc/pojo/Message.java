package com.yc.pojo;

/**
 * Created by Administrator on 2017/12/1.
 */
public class Message {
    public static final String SUCCESS = "1";
    public static final String FAILED = "0";
    private String desc;
    private Object data;
    private String code;

    public Message(String code, Object data, String desc) {
        this.code = code;
        this.data = data;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
