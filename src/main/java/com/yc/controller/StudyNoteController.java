package com.yc.controller;

import com.yc.pojo.Message;
import com.yc.pojo.StudyNote;
import com.yc.pojo.User;
import com.yc.service.IStudyNoteService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2017/11/29.
 */
@Controller
@RequestMapping("/study")
public class StudyNoteController {

    @Resource
    private IStudyNoteService studyNoteService;

    @ResponseBody
    @RequestMapping("/save")
    public Message save(@RequestParam StudyNote studyNote) throws Exception{
        Message message = new Message(Message.SUCCESS, null, "处理成功");
        return message;
    }

    @ResponseBody
    @RequestMapping("/delete")
    public Message delete(@RequestParam long userId, Model model) throws Exception{
        studyNoteService.deleteNote(userId);
        Message message = new Message(Message.SUCCESS, "", "处理成功");
        return message;
    }

    @ResponseBody
    @RequestMapping("/update")
    public Message update(@RequestParam StudyNote studyNote, Model model)throws Exception {
        studyNoteService.updateNote(studyNote);
        Message message = new Message(Message.SUCCESS, null, "处理成功");
        return message;
    }

    @ResponseBody
    @RequestMapping("/get")
    public Message get(@RequestParam long userId, Model model)throws Exception {
        StudyNote studyNote = studyNoteService.getNoteById(userId);
        Message message = new Message(Message.SUCCESS, studyNote, "处理成功");
        return message;
    }

    @ResponseBody
    @RequestMapping("/getList")
    public Message getList(@RequestParam long userId, @RequestParam int start, @RequestParam int page, Model model) throws Exception{
        List<StudyNote> studyNotes = studyNoteService.getNoteList(userId, start, page);
        Message message = new Message(Message.SUCCESS, studyNotes, "处理成功");
        return message;
    }


}
