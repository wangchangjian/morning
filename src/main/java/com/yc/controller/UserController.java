package com.yc.controller;

import com.yc.pojo.Message;
import com.yc.pojo.User;
import com.yc.service.IUserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by Administrator on 2017/11/29.
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Resource
    private IUserService userService;

    @ResponseBody
    @RequestMapping("/showUser")
    public Message showUser(@RequestParam long userId, Model model) {
        User user = userService.getUserById(userId);
        Message message = new Message(Message.SUCCESS, user, "处理成功");
        return message;
    }
}
