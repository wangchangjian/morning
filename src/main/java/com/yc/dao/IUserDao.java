package com.yc.dao;

import com.yc.pojo.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2017/11/29.
 */
@Repository
public interface IUserDao {
    List<User> selectByPrimaryKey(@Param("userId") long userId);
}
