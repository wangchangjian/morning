package com.yc.dao;

import com.yc.pojo.StudyNote;
import com.yc.pojo.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2017/11/29.
 */
@Repository
public interface IStudyNoteDao {
    StudyNote selectByPrimaryKey(@Param("id") long id);

    void delete(@Param("id") long id);

    void add(@Param("id") StudyNote id);

    void update(@Param("id") StudyNote id);

    List<StudyNote> getNoteList(@Param("userId") long userId, @Param("start") int start, @Param("page") int page);
}
