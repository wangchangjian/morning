package com.yc.service;

import com.yc.pojo.User;

/**
 * Created by Administrator on 2017/11/29.
 */
public interface IUserService {
     User getUserById(long userId);
}
