package com.yc.service;

import com.yc.pojo.StudyNote;

import java.util.List;

/**
 * Created by Administrator on 2017/11/29.
 */
public interface IStudyNoteService {
    StudyNote getNoteById(long userId);

    List<StudyNote> getNoteList(long userId, int start, int page);

    void deleteNote(long userId);

    void addNote(StudyNote studyNote);

    void updateNote(StudyNote studyNote);
}
