package com.yc.service.serviceImp;

import com.yc.dao.IStudyNoteDao;
import com.yc.pojo.StudyNote;
import com.yc.service.IStudyNoteService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2017/11/29.
 */

@Service("studyNoteService")
public class StudyNoteService implements IStudyNoteService {
    @Resource
    private IStudyNoteDao userDao;

    public StudyNote getNoteById(long id) {
        return userDao.selectByPrimaryKey(id);
    }

    public List<StudyNote> getNoteList(long userId, int start, int page) {
        return userDao.getNoteList(userId,start, page);
    }

    public void deleteNote(long id) {
        userDao.delete(id);

    }

    public void addNote(StudyNote studyNote) {
        userDao.add(studyNote);

    }

    public void updateNote(StudyNote studyNote) {
        userDao.update(studyNote);

    }
}
