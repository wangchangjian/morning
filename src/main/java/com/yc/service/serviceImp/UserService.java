package com.yc.service.serviceImp;

import com.yc.dao.IUserDao;
import com.yc.pojo.User;
import com.yc.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Administrator on 2017/11/29.
 */

@Service("userService")
public class UserService implements IUserService {

    @Resource
    private IUserDao userDao;


    public User getUserById(long userId) {
        return userDao.selectByPrimaryKey(userId).get(0);
    }
}
